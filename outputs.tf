
# output "info" {
#   value = aws_instance.test[*]
# }

# output "vm_list" {
#   value = toset(compact([for k in split(",", replace(var.vm_list, " ", "")) : k]))
# }

# output "ami_id" { value = var.is_end ? aws_ami_from_instance.make_ami["${var.ami_name}"].id : "ami_not_ready" }
output "aws_rhel7_ami_id" {
  value = length(data.aws_ami_ids.rhel7.ids) > 0 ? data.aws_ami_ids.rhel7.ids[0] : ""
}

output "aws_rhel8_ami_id" {
  value = length(data.aws_ami_ids.rhel8.ids) > 0 ? data.aws_ami_ids.rhel8.ids[0] : ""
}

output "runfirst" { value = terraform_data.run_first }
output "varstatus" { value = var.status }