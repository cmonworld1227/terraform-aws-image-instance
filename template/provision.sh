#!/bin/sh
echo "============= Partition Fix =============="
printf "fix\n" | sudo parted ---pretend-input-tty /dev/nvme0n1 print
printf "fix\n" | sudo parted ---pretend-input-tty /dev/nvme0n1 print
sudo parted -l

echo "============= Partition Check (ROOT) =============="
sudo df -Th | grep "^/dev"
sudo file -s /dev/nvme0n1p2

echo "============= Partitioning... =============="
# /boot/efi(1G) /swap(600G) /var(20G) /tmp(remain all)
sudo parted --script /dev/nvme0n1 -- \
    mkpart primary xfs 30GiB 34GiB \
    mkpart primary xfs 34GiB 640GiB \
    mkpart primary 640GiB 660GiB \
    mkpart primary xfs 660GiB -1
sudo lsblk

echo "============= Partition format xfs/swap =============="
sudo mkfs -t xfs /dev/nvme0n1p3
sudo xfs_admin -L EFI /dev/nvme0n1p3

# sudo mkfs -L SWAP -t xfs /dev/nvme0n1p4
sudo mkswap -L SWAP /dev/nvme0n1p4

sudo mkfs -t xfs /dev/nvme0n1p5
sudo xfs_admin -L VAR /dev/nvme0n1p5

sudo mkfs -t xfs /dev/nvme0n1p6
sudo xfs_admin -L TMP /dev/nvme0n1p6

sudo parted -l

echo "============= Check temp directory =============="
sudo mkdir /var.new
sudo mkdir /boot/efi.new
sudo mkdir /swap
sudo mkdir /tmp.new
sudo ls /boot/efi /var /tmp

echo "============= Partition Mount =============="
# /boot/efi(1G)
sudo mount /dev/nvme0n1p3 /boot/efi.new
sudo rsync -raX /boot/efi/ /boot/efi.new/
echo 'LABEL=EFI    /boot/efi    xfs    defaults    0 0' | sudo tee -a /etc/fstab

# /swap(600G)
echo 'LABEL=SWAP    swap    swap    defaults    0 0' | sudo tee -a /etc/fstab
sudo swapon -a
sudo swapon -s

# /var(20G)
sudo mount /dev/nvme0n1p5 /var.new
sudo rsync -raX /var/ /var.new/
echo 'LABEL=VAR    /var    xfs    defaults    0 0' | sudo tee -a /etc/fstab

# /tmp(remain all)
sudo mount /dev/nvme0n1p6 /tmp.new
sudo rsync -raX /tmp/ /tmp.new/
echo 'LABEL=TMP    /tmp    xfs    defaults    0 0' | sudo tee -a /etc/fstab

echo "============= Disk Free Check Mount =============="
sudo df -h

echo "============= Package Update =============="
# sudo yum update -y

# echo "============= Reboot =============="
# sudo reboot