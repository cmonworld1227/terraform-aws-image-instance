packer {
  required_plugins {
    amazon = {
      version = ">= 1.2.5"
      source  = "github.com/hashicorp/amazon"
    }
    azure = {
      version = ">= 1.4.2"
      source  = "github.com/hashicorp/azure"
    }
  }
}

source "amazon-ebs" "rhel7" {
  ami_name                = var.aws_rhel7_ami_name
  ami_virtualization_type = "hvm"
  instance_type           = "t3.micro"
  source_ami              = var.aws_rhel7_id
  ssh_username            = "ec2-user"
    // root - /swap(600G) 
  launch_block_device_mappings {
    device_name           = "/dev/sda1"
    snapshot_id           = var.snapshot_id
    volume_type           = "gp2"
    delete_on_termination = true
  }
  ssh_timeout             = "10m"
  force_delete_snapshot   = true
  ami_regions             = ["ap-northeast-2"]
  region                  = "ap-northeast-2"
  ena_support             = true
  tags = {
    Revision      = "$${var.postfix}"
    Base_AMI_Name = "{{ .SourceAMIName }}"
    CreationDate  = formatdate("YYYYMMDD_hhmmss", timeadd(timestamp(), "9h"))
  }
}

source "amazon-ebs" "rhel8" {
  ami_name                = var.aws_rhel8_ami_name
  ami_virtualization_type = "hvm"
  instance_type           = "t3.micro"
  source_ami              = var.aws_rhel8_id
  ssh_username            = "ec2-user"
  ssh_timeout             = "60m"
  force_delete_snapshot   = true
  ami_regions             = ["ap-northeast-2"]
  region                  = "ap-northeast-2"


  // // root - /swap(600G) 
  // launch_block_device_mappings {
  //   device_name           = "/dev/sda1"
  //   volume_size           = 630
  //   volume_type           = "gp2"
  //   delete_on_termination = true
  // }

  // // other - /boot/efi(1G) /var(20G) /tmp
  // launch_block_device_mappings {
  //   device_name           = "/dev/sdb"
  //   volume_size           = 1158
  //   volume_type           = "gp2"
  //   delete_on_termination = true
  // }

  tags = {
    Revision      = "$${var.postfix}"
    Base_AMI_Name = "{{ .SourceAMIName }}"
    CreationDate  = formatdate("YYYYMMDD_hhmmss", timeadd(timestamp(), "9h"))
  }
}


///////////////////////////////////////////////////////////////////////////////////


# Basic example : https://www.packer.io/docs/builders/azure/arm#basic-example
# MS Guide : https://docs.microsoft.com/ko-kr/azure/virtual-machines/linux/build-image-with-packer
// source "azure-arm" "rhel7" {

//   client_id       = var.client_id
//   client_secret   = var.client_secret
//   tenant_id       = var.tenant_id
//   subscription_id = var.subscription_id

//   managed_image_name                = "$${var.image_name}-test2"
//   managed_image_resource_group_name = var.resource_group_name
//   build_resource_group_name         = var.resource_group_name
  
//   os_disk_size_gb = 1788
//   os_type         = "Linux"
//   image_publisher = "RedHat"
//   image_offer     = "RHEL"
//   image_sku       = "7.6"



//   azure_tags = {
//     dept = "KBHC Terraform POC"
//   }
//   vm_size = "Standard_A2_v2"

// }


///////////////////////////////////////////////////////////////////////////////////


build {
  sources = [
    "source.amazon-ebs.rhel7",
    //"source.amazon-ebs.rhel8",
    //"source.azure-arm.rhel7"
  ]

  // provisioner "shell" {
  //   script            = "provision.sh"
  //   timeout           = "120m"
  //   max_retries       = 0
  //   expect_disconnect = true
  // }

  provisioner "shell" {
    script              = "cleanup.sh"
    start_retry_timeout = "5m"
    pause_before        = "1m"
    max_retries         = 0
  }
}
