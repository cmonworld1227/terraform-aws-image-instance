terraform {
  cloud {
    organization = "ddimtech-samsung-poc"
    hostname     = "app.terraform.io"
    workspaces {
      name = "terraform-goldenImage-ws-rhel7"
    }
  }
  required_providers {
    tfe = {
      version = "~> 0.45.0"
    }
    gitlab = {
      source  = "gitlabhq/gitlab"
      version = "16.0.3"
    }
    aws = {
      version = "5.0.1"
    }
  }
}
provider "aws" {
  region = "ap-northeast-2"
}

provider "gitlab" {
  //base_url = "https://git.ddim.com/api/v4/"
  token = var.gitlab_token
}

locals {}

//////////Run ///////////////////

resource "terraform_data" "run_first" {
  depends_on = [
    aws_instance.ami_instance,
    terraform_data.init_to_provision
  ]
  count = var.status == "init" ? 1 : 0
  triggers_replace = [
    timestamp()
  ]
  provisioner "local-exec" {
    command = <<-EOT
  curl --request POST --header "Authorization: Bearer ${var.token}" --header "Content-Type: application/vnd.api+json" --data '{
  "data": {
    "attributes": {
      "auto-apply": "true"
    },
    "type": "runs",
    "relationships": {
      "workspace": {
        "data": {
          "type": "workspaces",
          "id": "${var.workspace_id}"
        }
      }
    }
  }
}' https://app.terraform.io/api/v2/runs
    EOT
  }
}

resource "terraform_data" "run_second" {
  # depends_on = [terraform_data.init_to_provision]
  depends_on = [terraform_data.provision_to_stop]
  count      = var.status == "provision" ? 1 : 0
  triggers_replace = [
    timestamp()
  ]
  provisioner "local-exec" {
    command = <<-EOT
  curl --request POST --header "Authorization: Bearer ${var.token}" --header "Content-Type: application/vnd.api+json" --data '{
  "data": {
    "attributes": {
      "auto-apply": "true"
    },
    "type": "runs",
    "relationships": {
      "workspace": {
        "data": {
          "type": "workspaces",
          "id": "${var.workspace_id}"
        }
      }
    }
  }
}' https://app.terraform.io/api/v2/runs
    EOT
  }
}

resource "terraform_data" "run_third" {
  depends_on = [terraform_data.provision_to_stop]
  count      = var.status == "stop" ? 1 : 0
  # triggers_replace = [
  #   timestamp()
  # ]
  provisioner "local-exec" {
    command = <<-EOT
  curl --request POST --header "Authorization: Bearer ${var.token}" --header "Content-Type: application/vnd.api+json" --data '{
  "data": {
    "attributes": {
      "auto-apply": "true"
    },
    "type": "runs",
    "relationships": {
      "workspace": {
        "data": {
          "type": "workspaces",
          "id": "${var.workspace_id}"
        }
      }
    }
  }
}' https://app.terraform.io/api/v2/runs
    EOT
  }
}

//${var.workspace_id}/actions/apply

# ///////////////////////////////////// AWS /////////////////////////////////


resource "aws_instance" "ami_instance" {
  depends_on = []
  count      = var.status == "init" || var.status == "provision" ? 1 : 0

  associate_public_ip_address = true

  ami           = var.base_ami
  instance_type = var.vm_type
  subnet_id     = var.subnet_id

  vpc_security_group_ids = [var.security_group_id]
  key_name               = "JIN"

  root_block_device {
    volume_size = var.status == "provision" ? 1900 : 30
    volume_type = "gp2"
  }

  tags = {
    Name = "${var.ami_name}-snapshot-instance"
    hyc  = "STG-EC2"
  }
}

resource "terraform_data" "script" {
  depends_on = [aws_instance.ami_instance]
  count      = var.status == "provision" ? 1 : 0
  connection {
    private_key = var.private_key
    type        = "ssh"
    user        = "ec2-user"
    host        = aws_instance.ami_instance[0].public_ip
  }

  provisioner "remote-exec" {
    script     = "${path.module}/scripts/provision.sh"
    on_failure = continue
  }

  provisioner "local-exec" {
    command = "sleep 30s"
  }

  provisioner "remote-exec" {
    script = "${path.module}/scripts/cleanup.sh"
  }
  ## provisioner "remote-exec" { Insert install.sh}
}

resource "aws_ebs_snapshot" "example_snapshot" {
  depends_on = [
    terraform_data.script
  ]
  count     = var.status == "provision" ? 1 : 0
  volume_id = aws_instance.ami_instance[0].root_block_device.0.volume_id

  tags = {
    Name = "imsi-snapshot"
  }
}

resource "aws_ami" "make_ami" {
  depends_on = [
    aws_ebs_snapshot.example_snapshot,
    terraform_data.script
  ]
  count               = var.status == "provision" ? 1 : 0
  name                = "make_ami-terraform"
  virtualization_type = "hvm"
  root_device_name    = "/dev/sda1"
  imds_support        = "v2.0"
  ena_support         = true

  ebs_block_device {
    device_name = "/dev/sda1"
    snapshot_id = aws_ebs_snapshot.example_snapshot[0].id
    volume_size = 1900
  }
}

# resource "aws_ami_from_instance" "make_ami" {
#   depends_on = [
#     aws_instance.ami_instance,
#     terraform_data.script
#   ]
#   count = var.status == "provision" ? 1 : 0

#   name               = "${var.OS}-${var.ami_name}"
#   source_instance_id = aws_instance.ami_instance[0].id
# }


////////////////////////////////////// Packer //////////////////////////////////////


resource "template_dir" "packer" {
  depends_on = [
    aws_ami.make_ami,
    aws_instance.ami_instance
  ]

  count           = var.status == "provision" ? 1 : 0
  source_dir      = "${path.module}/template"
  destination_dir = "${path.module}/packer"
  vars = {
    revision           = var.revision
    aws_rhel7_ami_name = local.aws_rhel7_ami_name
    aws_rhel7_id       = aws_ami.make_ami[0].id
    aws_rhel8_ami_name = local.aws_rhel8_ami_name
    aws_rhel8_id       = var.aws_rhel8_id
    snapshot_id        = aws_ebs_snapshot.example_snapshot[0].id

  }
}

resource "terraform_data" "run_packer" {
  depends_on = [
    aws_instance.ami_instance,
    aws_ami.make_ami,
    template_dir.packer
  ]
  count = var.status == "provision" ? 1 : 0

  triggers_replace = [
    var.revision,
    timestamp()
  ]

  provisioner "local-exec" {
    command     = "curl -L -o packer.zip ${var.packer_download_url}"
    working_dir = "${path.module}/packer"
  }

  provisioner "local-exec" {
    command     = "unzip ./packer.zip"
    working_dir = "${path.module}/packer"
  }

  provisioner "local-exec" {
    command     = "mkdir -p plugins/github.com/hashicorp/{amazon,azure}"
    working_dir = "${path.module}/packer"
  }

  provisioner "local-exec" {
    command     = "curl -L -o packer-plugin-amazon.zip ${var.packer_aws_plugin_download_url}"
    working_dir = "${path.module}/packer"
  }

  provisioner "local-exec" {
    command     = "curl -L -o packer-plugin-azure.zip ${var.packer_azure_plugin_download_url}"
    working_dir = "${path.module}/packer"
  }

  provisioner "local-exec" {
    command     = "unzip ./packer-plugin-amazon.zip -d plugins/github.com/hashicorp/amazon/"
    working_dir = "${path.module}/packer"
  }

  provisioner "local-exec" {
    command     = "unzip ./packer-plugin-azure.zip -d plugins/github.com/hashicorp/azure/"
    working_dir = "${path.module}/packer"
  }

  provisioner "local-exec" {
    command     = "rm -rf *.zip"
    working_dir = "${path.module}/packer"
  }

  provisioner "local-exec" {
    command     = "chmod 777 ./packer"
    working_dir = "${path.module}/packer"
  }
  provisioner "local-exec" {
    command     = "uname -an"
    working_dir = "${path.module}/packer"
  }
  provisioner "local-exec" {
    command     = "ls -rtl ."
    working_dir = "${path.module}/packer"
  }
  /*
  provisioner "local-exec" {
    command     = "./packer fmt ."
    working_dir = "${path.module}/packer"
  }*/
  provisioner "local-exec" {
    command     = "./packer init ."
    working_dir = "${path.module}/packer"
  }

  provisioner "local-exec" {
    command     = "./packer validate ."
    working_dir = "${path.module}/packer"

    environment = {
      PACKER_HOME_DIR = abspath("${path.module}/packer")
    }
  }

  provisioner "local-exec" {
    command     = "./packer build ."
    working_dir = "${path.module}/packer"

    environment = {
      PACKER_HOME_DIR = abspath("${path.module}/packer")
    }
  }
}

data "aws_ami_ids" "rhel7" {
  depends_on = [
    terraform_data.run_packer
  ]

  name_regex = "^aws-rhel7-\\d{4}(0?[1-9]|1[012])(0?[1-9]|[12][0-9]|3[01])-\\d{2}"
  owners     = ["self"]

  filter {
    name   = "name"
    values = ["aws-rhel7-*"]
  }
}

data "aws_ami_ids" "rhel8" {
  depends_on = [
    terraform_data.run_packer
  ]

  name_regex = "^aws-rhel8-\\d{4}(0?[1-9]|1[012])(0?[1-9]|[12][0-9]|3[01])-\\d{2}"
  owners     = ["self"]

  filter {
    name   = "name"
    values = ["aws-rhel8-*"]
  }
}


////////////////////////////////////////////////////////////////


resource "terraform_data" "init_to_provision" {
  # depends_on = [terraform_data.run_first]
  depends_on = [aws_instance.ami_instance]
  count      = var.status == "init" ? 1 : 0
  provisioner "local-exec" {
    command = <<-EOT
  curl --request PATCH --header "Authorization: Bearer ${var.token}" --header "Content-Type: application/vnd.api+json" --data '{
  "data": {
    "id":"var-hSeMZGCVEqJ3x4NJ",
    "attributes": {
      "key":"status",
      "value":"provision",
      "description": "new description",
      "category":"terraform",
      "hcl": false,
      "sensitive": false
    },
    "type":"vars"
  }
}' https://app.terraform.io/api/v2/vars/var-hSeMZGCVEqJ3x4NJ
   EOT
  }
}
//var-hSeMZGCVEqJ3x4NJ
resource "terraform_data" "provision_to_stop" {
  depends_on = [
    terraform_data.run_packer,
    template_dir.packer
  ]
  count = var.status == "provision" ? 1 : 0
  provisioner "local-exec" {
    command = <<-EOT
    curl --request PATCH --header "Authorization: Bearer ${var.token}" --header "Content-Type: application/vnd.api+json" --data '{
  "data": {
    "id":"var-hSeMZGCVEqJ3x4NJ",
    "attributes": {
      "key":"status",
      "value":"stop",
      "description": "new description",
      "category":"terraform",
      "hcl": false,
      "sensitive": false
    },
    "type":"vars"
  }
}' https://app.terraform.io/api/v2/vars/var-hSeMZGCVEqJ3x4NJ
   EOT
  }

}