
locals {
  aws_rhel7_ami_name = "aws-rhel7-${var.revision}"
  aws_rhel8_ami_name = "aws-rhel8-${var.revision}"
}

variable "revision" {
  type        = string
  description = "yyyymmdd-##"

  validation {
    condition     = can(regex("\\d{4}(0?[1-9]|1[012])(0?[1-9]|[12][0-9]|3[01])-\\d{2}", var.revision))
    error_message = "The image revision value must be a valid, \"yyyymmdd-##\"."
  }
}

variable "packer_download_url" {
  # //  default = "https://releases.hashicorp.com/packer/1.8.7/packer_1.8.7_darwin_amd64.zip"
  #   default = "https://releases.hashicorp.com/packer/1.8.7/packer_1.8.7_linux_amd64.zip"
}

variable "packer_aws_plugin_download_url" {
  # //default= "github.com/hashicorp/packer-plugin-amazon/releases/download/v1.2.5/packer-plugin-amazon_v1.2.5_x5.0_darwin_amd64.zip"
  # default = "https://github.com/hashicorp/packer-plugin-amazon/releases/download/v1.2.5/packer-plugin-amazon_v1.2.5_x5.0_linux_amd64.zip"
}
variable "packer_azure_plugin_download_url" {}

variable "aws_rhel7_id" {
  type        = string
  default     = "ami-004ed0ba77095ba25"
  description = "aws ec2 describe-images --owners 309956199498 --query 'sort_by(Images, &CreationDate)[*].[CreationDate,Name,ImageId]' --filters 'Name=name,Values=RHEL-7*' --region ap-northeast-2 --output table"

  validation {
    condition = (
      length(var.aws_rhel7_id) > 4 &&
      substr(var.aws_rhel7_id, 0, 4) == "ami-"
    )
    error_message = "The aws_rhel_id value must start with \"ami-\"."
  }
}
variable "region" {
  type    = string
  default = "ap-northeast-2"
}
variable "aws_rhel8_id" {
  type        = string
  default     = "ami-04c278b897996f966"
  description = "aws ec2 describe-images --owners 309956199498 --query 'sort_by(Images, &CreationDate)[*].[CreationDate,Name,ImageId]' --filters 'Name=name,Values=RHEL-8*' --region ap-northeast-2 --output table"

  validation {
    condition = (
      length(var.aws_rhel8_id) > 4 &&
      substr(var.aws_rhel8_id, 0, 4) == "ami-"
    )
    error_message = "The aws_rhel_id value must start with \"ami-\"."
  }
}

