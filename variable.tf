//variable "access_key" {}
//variable "secret_key" {}
//variable "ami" {}
//variable "instance_type" {}

variable "ami_name" {
  type = string
}

variable "base_ami" {
  type = string
}

variable "vm_type" {
  type = string
}

variable "token" {
  type = string
}

variable "gitlab_token" {
  type = string
}

variable "security_group_id" {
  type = string
}

variable "is_provision" {
  type    = bool
  default = false
}

variable "is_end" {
  type    = bool
  default = false
}

variable "subnet_id" {
  type = string
}

variable "OS" {
  type = string
}

variable "private_key" {
  type = string
}
variable "status" {
  type        = string
  description = "run status"
}

variable "workspace_id" {
  type = string
}


////