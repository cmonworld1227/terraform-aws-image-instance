revision                         = "20230530-01"
packer_download_url              = "https://releases.hashicorp.com/packer/1.8.7/packer_1.8.7_linux_amd64.zip"
packer_aws_plugin_download_url   = "https://github.com/hashicorp/packer-plugin-amazon/releases/download/v1.2.5/packer-plugin-amazon_v1.2.5_x5.0_linux_amd64.zip"
packer_azure_plugin_download_url = "https://github.com/hashicorp/packer-plugin-azure/releases/download/v1.4.2/packer-plugin-azure_v1.4.2_x5.0_linux_amd64.zip"


# //packer 플러그인 다운로드, 고객사 방문 시 필요한 파일 전달 사항
# packer_download_url              = "https://releases.hashicorp.com/packer/1.8.7/packer_1.8.7_darwin_amd64.zip"
# packer_aws_plugin_download_url   = "github.com/hashicorp/packer-plugin-amazon/releases/download/v1.2.5/packer-plugin-amazon_v1.2.5_x5.0_darwin_amd64.zip"
# packer_azure_plugin_download_url = "github.com/hashicorp/packer-plugin-azure/releases/download/v1.4.2/packer-plugin-azure_v1.4.2_x5.0_darwin_amd64.zip"

///////// AWS /////////
// AWS_ACCESS_KEY_ID="anaccesskey"
// AWS_SECRET_ACCESS_KEY="asecretkey"
AWS_DEFAULT_REGION = "ap-northeast-2"

//고객사 베이스이미지 삽입
aws_rhel7_id = "ami-0708fd0ae9a663e02"
aws_rhel8_id = "ami-04c278b897996f966"

//ami-004ed0ba77095ba25

//ARM_SUBSCRIPTION_ID   = ""
//ARM_TENANT_ID         = ""
//ARM_CLIENT_ID         = ""
//ARM_CLIENT_SECRET     = ""
# virtual_network_name                = "5247e007-55d8-46d4-9768-3633500ea99f"
# virtual_network_subnet_name         = "virtualNetwork1"
# virtual_network_resource_group_name = "ddimtech_resource_group_1"


resource_group_name = "samsung-poc-azure-network-rg"
